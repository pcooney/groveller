from colours import *

class OddColouredFilter:
	def __init__(self, colour):
		self.colour = colour

	def filter(self, index, datum):
		if index % 2 > 0:
			return self.colour
		else:
			return None

class RangeFilter:
	def __init__(self, colour, start, end):
		if start >= end or start < 0:
			raise Exception("bad indices")

		self.colour = colour
		self.start = start
		self.end = end

	def filter(self, index, datum):
		if self.start <= index and index <= self.end:
			return self.colour
		else:
			return None


