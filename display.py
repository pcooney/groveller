from printer import bcolours

class Display:

	def __init__(self, data, items_per_line = 16, filters = [], lifters = [], delim = " "):
		self.data = data
		self.items_per_line = items_per_line
		self.filters = filters
		self.lifters = lifters
		self.delim = delim

	
	def get_filter_colour(self, index, datum):
		if not self.filters:
			return None

		for filter in self.filters:
			colour = filter.filter(index, datum)
			if colour:
				return colour
		return None

	def draw_lifters(self):
		for lifter in self.lifters:
			print lifter

	def draw_main(self):
		count = 0
		line = ""

		for i in range(0, len(self.data)):
			item = self.data[i]
			colour = self.get_filter_colour(i, item)
			part = item
			if colour:
				part = colour + part + bcolours.ENDC

			line += self.delim + part

			# increment the count and flush
			count += 1
			if (count % self.items_per_line == 0):
				print line
				line = ""

		if line:
			print line

	def draw(self):
		self.draw_main()
		self.draw_lifters()
