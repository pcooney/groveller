from encoders import *
from display import Display
from filters import *
from colours import *
from lifters import *
data = "abcdefghijklmnopqrstuvwxyz"
encoder = HexEncoder()

#todo how to make the lifted values understand the line layout, and encoding, as they run over bytes?
lifters = [IntLifter(data, 7), IntLifter(data, 11)]

display = Display(encoder.encode(data), 4, [RangeFilter(OKBLUE, 2, 7), OddColouredFilter(OKGREEN)], lifters,  "\t")

display.draw()

