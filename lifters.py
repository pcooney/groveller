import struct

class IntLifter:
	def __init__(self, data, start):
		self.data = data
		self.start = start

	def is_valid(self):
		return self.start + 3 < len(self.data)
	
	def get_start_index(self):
		return self.start

	def get_value(self):
		if self.is_valid():
			return struct.unpack("i", self.data[self.start:self.start + 4])[0]
		else:
			return 0

	def __repr__(self):
		return "int32 @%d=>%d" % (self.start, self.get_value())
