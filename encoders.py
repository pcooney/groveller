class ByteEncoder:

	def encode(data):
		return [x for x in data]

class HexEncoder:

	def encode(self, data):
		return [x.encode("hex") for x in data]
	
class BitEncoder:
	def bits(self, item):
		bitstr = ""
		for i in range(0, 8):
			if item & i == 1:
				bitstr += "1"
			else:
				bitstr += "0"
			
			item = item >> 1
		return bitstr

	def encode(self, data):
		return [self.bits(ord(x)) for x in data] 
